### Library

A library (stored in `library.csv`) is being kept up-to-date using a Typescript CLI tool.

Before compiling we need to install dependencies using `npm install`.
Compile running `tsc` (or `tsc --watch` while developing). If you don't have `tsc` installed, you can install it using `npm i --global typescript`.

Run the compiled code with `node dist/library.js`.

Choose option 1 or 2 when prompted to add a book or simply print the entire library.
An example library has been provided.
