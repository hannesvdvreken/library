import * as readline from "readline";
import Table from "cli-table";
import {Book} from "./book";
import {IDB} from "./db";

export class Add {
  constructor(
    private db: IDB,
    private input: NodeJS.ReadableStream,
    private output: NodeJS.WritableStream
  ) {}

  async run() {
    let book: Book = (await this.ask() as Book)
    this.print(book);
    this.store(book);
  }

  private async ask() {
    let ask = readline.createInterface({
      input: this.input,
      output: this.output
    });

    const question1 = () => {
      return new Promise((resolve) => {
        ask.question('What is the name of the book: ', (title) => {
          resolve(title);
        })
      })
    };

    const question2 = () => {
      return new Promise((resolve) => {
        ask.question('Who is the author: ', (author) => {
          resolve(author)
        })
      })
    };

    const question3 = () => {
      return new Promise((resolve) => {
        ask.question('What is it\'s ISBN number: ', (isbn) => {
          resolve(isbn)
        })
      })
    }

    let title = await question1();
    let author = await question2();
    let isbn = await question3();

    ask.close();

    return new Promise(resolve => {
      resolve({title, author, isbn});
    });
  }

  private print(book: Book) {
    let table = new Table();
    table.push([book.title, book.author, book.isbn]);

    this.output.write("Adding this book:\n" + table.toString() + "\n");
  }

  private async store(book: Book) {
    let books: Book[] = await this.db.retrieve();
    books.push(book);
    await this.db.store(books);
  }
}
