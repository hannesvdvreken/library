import Table from "cli-table";
import {IDB} from "./db";

export class Printer {
  constructor(private db: IDB, private output: NodeJS.WriteStream) {}

  async print() {
    let books = await this.db.retrieve();

    let table = new Table();

    for (let book of books) {
      table.push([book.title, book.author, book.isbn]);
    }

    this.output.write(table.toString() + "\n");
  }
}
