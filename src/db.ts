import {Book} from "./book";
import fs from "fs";
import parser from "csv-parse";
import stringify from "csv-stringify";

interface RetrieveFn {
  (): Promise<Book[]>
}

interface StoreFn {
  (books: Book[]): Promise<null>
}

export interface IDB {
  retrieve: RetrieveFn,
  store: StoreFn;
}

export class DB implements IDB {
  constructor(private filename: string) {}

  async retrieve(): Promise<Book[]> {
    return new Promise((resolve, reject) => {
      fs.readFile(this.filename, (error, data) => {
        if (error) {
          reject(error);
        }

        parser(data, {
          trim: true,
          skip_empty_lines: true,
          delimiter: ';'
        }, (error, records: [], info) => {
          if (error) {
            reject(error);
          }

          resolve(records.map((record: any[]) => {
            return {
              title: (record[0] as string),
              author: (record[1] as string),
              isbn: (record[2] as number),
            }
          }));
        });
      });
    });
  }

  async store(books: Book[]): Promise<null> {
    // Sorting the books by author.
    books = books.sort((a: Book, b: Book) => {
      return a.author.toLowerCase() > b.author.toLowerCase()? 1: (a.author.toLowerCase() == b.author.toLowerCase() ? 0 : -1);
    });

    // Saving to disk.
    return new Promise((resolve, reject) => {
      stringify(books.map((book: Book) => {
        return [book.title, book.author, book.isbn];
      }), {
        delimiter: ";",
      }, (error, output) => {
        if (error) {
          reject(error);
        }

        fs.writeFile(this.filename, output, (error) => {
          if (error) {
            reject(error);
          }

          resolve(null);
        });
      });
    });
  }
}
