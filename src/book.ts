export type Book = {
  title: string,
  author: string,
  isbn: number
}
