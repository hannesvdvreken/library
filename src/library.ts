import * as readline from "readline";
import {Printer} from "./print";
import {Add} from "./add";
import {DB} from "./db";

const filename = 'library.csv';

let read = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

read.question('Do you want to (1) add a new book, (2) print library or (Q) quit: ', async (answer) => {
  read.close();
  switch (answer.toLowerCase()) {
    case '1':
      let add = new Add(new DB(filename), process.stdin, process.stdout);
      await add.run();
      break;
    case '2':
      let printer = new Printer(new DB(filename), process.stdout);
      await printer.print();
      break;
    default:
      console.log('Bye!');
      break;
  }
});

